'use strict';

/**
 * @ngdoc service
 * @name demoEstebanApp.service
 * @description
 * # service
 * Service in the demoEstebanApp.
 */
angular.module('app.services',[])

  .service('service', function ($q,$http) {
    
    // Servicio para obtener los datos del JSON que esta en Data/database.json
    this.getData = function(){

      var deferred = $q.defer();
      var that     = this;

      $http({       
        method:'GET',
        url: '../data/database.json'
      }).then(function successCallback(response) {
        deferred.resolve(response);
      }, function errorCallback(response) {
        deferred.reject(response);
      });

      return deferred.promise;  


    }

  });
