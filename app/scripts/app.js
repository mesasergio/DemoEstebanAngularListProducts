'use strict';

/**
 * @ngdoc overview
 * @name demoEstebanApp
 * @description
 * # demoEstebanApp
 *
 * Main module of the application.
 */
angular
  .module('demoEstebanApp', [
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'app.controller',
    'app.services'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
