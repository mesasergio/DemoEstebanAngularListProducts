'use strict';

/**
 * @ngdoc function
 * @name demoEstebanApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the demoEstebanApp
 */
angular.module('app.controller',[])

  .controller('MainCtrl', function (service) {

    // Declaro la instancia del controlador
    var that = this;

    // Arreglo que va a tener los productos
    that.productos = [];

    // Arreglo que va a tener el carrito
    that.cart = []

    // consulto la informacion de servicio que se llama service y la funcion getData
    service.getData().then(success,error);

    function success(res){
      // en el array res, los productos estan en data->products
      that.productos = res.data.products
      console.log(that.productos)
    }

    function error(err){
      console.log(err)
    }

    // Funcion para agregar a el carrito
    that.addToCart = function(item,index){
      var producto = item // Muevo el item a una variable para poder agregarla al array de cart
      producto.qty = 1 // a la variable le pongo como cantidad inicial 1
      producto.indexOrigin = index;
      that.productos[index].disabled  = true // deshabilito el boton de agregar al carrito
      that.cart.push(item) // Adicion el producto al array de cart
      console.log(producto)
    }

    that.plusItemCart = function(index){
      that.cart[index].qty += 1;
    }

    that.minusItemCart = function(index){
      if(that.cart[index].qty > 1) that.cart[index].qty -= 1;
    }

    that.removeItemCart = function(index){
      // Como habia guardado el indexOrigin(osea donde estaba el producto en la lista de producto), desbloqueo el agregar
      that.productos[that.cart[index].indexOrigin].disabled = false
      // Elimino el item de la lista de cart
      that.cart.splice(index, 1);
    }

  });
